package com.mobile.rawrcodes.doctorsearch.view;

import android.content.Context;
import android.os.Parcel;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.mobile.rawrcodes.doctorsearch.R;
import com.mobile.rawrcodes.doctorsearch.api.RestApiClient;
import com.mobile.rawrcodes.doctorsearch.model.Doctor;

import java.util.List;

public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.DoctorViewHolder> {
    private List<Doctor> mDoctorList;
    private OnDoctorSelectedListener mDoctorListener;
    ImageLoader mImageLoader;

    int displaySize;

    public interface OnDoctorSelectedListener {
        void onDoctorSelected(Doctor doctor);
    }

    public DoctorsAdapter(List<Doctor> doctorList, Context context) {
        displaySize = doctorList.size();
        this.mDoctorList = doctorList;
        mImageLoader = RestApiClient.sharedInstance(context.getApplicationContext()).getImageLoader();
    }

    @Override
    public DoctorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_doctor, parent, false);

        return new DoctorViewHolder(itemView, mDoctorListener);
    }

    @Override
    public void onBindViewHolder(DoctorViewHolder holder, int position) {
        Doctor doctor = mDoctorList.get(position);
        holder.networkImageView.setImageUrl(doctor.getImageUrl(), mImageLoader);
        holder.nameTextView.setText(doctor.getName());
        holder.areaTextView.setText(doctor.getArea());
        holder.currencyTextView.setText(doctor.getCurrency());
        holder.rateTextView.setText(String.valueOf(doctor.getRate()));
        if (doctor.getSpeciality().equalsIgnoreCase("Dentist")) {
            holder.specialityImageView.setImageResource(R.drawable.ic_dental);
        }
    }

    @Override
    public int getItemCount() {
        return displaySize;
    }

    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;
    }

    class DoctorViewHolder extends RecyclerView.ViewHolder {
        private NetworkImageView networkImageView;
        private TextView nameTextView, areaTextView, currencyTextView, rateTextView;
        private ImageView specialityImageView;

        public DoctorViewHolder(View view, final OnDoctorSelectedListener handler) {
            super(view);
            networkImageView = (NetworkImageView) view.findViewById(R.id.network_image);
            nameTextView = (TextView) view.findViewById(R.id.text_name);
            areaTextView = (TextView) view.findViewById(R.id.text_area);
            currencyTextView = (TextView) view.findViewById(R.id.text_currency);
            rateTextView = (TextView) view.findViewById(R.id.text_rate);
            specialityImageView = (ImageView) view.findViewById(R.id.image_speciality);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Doctor doctor = mDoctorList.get(position);
                    handler.onDoctorSelected(doctor);
                }
            });
        }
    }

    public void setDoctorListener(OnDoctorSelectedListener mDoctorListener) {
        this.mDoctorListener = mDoctorListener;
    }
}
