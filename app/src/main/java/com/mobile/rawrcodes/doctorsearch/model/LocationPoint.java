package com.mobile.rawrcodes.doctorsearch.model;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by Then on 21/11/2016.
 */

public class LocationPoint {
    private String area;
    private String city;

    public LocationPoint(@NonNull JSONObject jsonObject) throws Exception {
//        {
//            "area": "Kuta - Tuban",
//                "city": "Bali"
//        }

        area = jsonObject.optString("area");
        city = jsonObject.optString("city");
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
