package com.mobile.rawrcodes.doctorsearch.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by Then on 21/11/2016.
 */

public class Doctor implements Parcelable {
    private String id;
    private String name;
    private String area;
    private String speciality;
    private String currency;
    private double rate;
    private int recommendation;
    private String schedule;
    private int experience;
    private double latitude;
    private double longitude;
    private String imageUrl;
    private String description;

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
    }

    protected Doctor(Parcel in) {
        id = in.readString();
    }

    public static final Creator<Doctor> CREATOR = new Creator<Doctor>() {
        @Override
        public Doctor createFromParcel(Parcel in) {
            return new Doctor(in);
        }

        @Override
        public Doctor[] newArray(int size) {
            return new Doctor[size];
        }
    };
    //end region Parcelable

    public Doctor(@NonNull JSONObject jsonObject) throws Exception {
        // Doctor List
//        {
//            "id": 1,
//                "name": "Sundar Pichai",
//                "speciality": "Dentist",
//                "area": "Kebon Jeruk",
//                "currency": "IDR",
//                "rate": 250000,
//                "photo": "http://52.76.85.10/test/photo.jpg"
//        }

        // Doctor Profile
//        {
//            "id": 20,
//                "name": "Bailey Kaelyn",
//                "speciality": "Dentist",
//                "area": "Kebon Jeruk",
//                "currency": "IDR",
//                "rate": 200000,
//                "recommendation": 902,
//                "schedule": "Weekdays 10AM-18PM",
//                "experience": 9,
//                "latitude": -6.195942,
//                "longitute": 106.773595,
//                "photo": "http://52.76.85.10/test/photo.jpg",
//                "description": "Dr. Bailey Kaelyn has an excellent eye for detail and is greatly revered by his patients. Having completed Masters in Pediatric and Preventive Dentistry, he also has multiple Certifications in Cosmetic Dentistry including those from the likes of NYU."
//        }

        id = jsonObject.optString("id");
        name = jsonObject.optString("name");
        speciality = jsonObject.optString("speciality");
        area = jsonObject.optString("area");
        currency = jsonObject.optString("currency");
        rate = jsonObject.optInt("rate");

        recommendation = jsonObject.optInt("recommendation");
        schedule = jsonObject.optString("schedule");
        experience = jsonObject.optInt("experience");
        latitude = jsonObject.optDouble("latitude");
        longitude = jsonObject.optDouble("longitute");

        imageUrl = jsonObject.optString("photo");

        description = jsonObject.optString("description");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(int recommendation) {
        this.recommendation = recommendation;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
