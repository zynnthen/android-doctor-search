package com.mobile.rawrcodes.doctorsearch.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobile.rawrcodes.doctorsearch.R;
import com.mobile.rawrcodes.doctorsearch.api.RestApiClient;
import com.mobile.rawrcodes.doctorsearch.model.Doctor;

public class DoctorProfileFragment extends Fragment implements OnMapReadyCallback {
    public static String TAG = DoctorProfileFragment.class.getSimpleName();

    private static final String ARG_PARAM_DOCTOR = "doctorParam";

    private Doctor doctor;
    private ImageLoader mImageLoader;
    private GoogleMap mGoogleMap;
    private LatLng mLatLng;

    // UI
    TextView mErrorTextView;
    RelativeLayout mContentRelativeLayout;
    NetworkImageView mProfileNetworkImageView;
    ImageView mSpecialityImageView;
    RatingBar mRatingBar;
    TextView mRecommendationTextView, mNameTextView, mAreaTextView, mScheduleTextView,
            mCurrencyTextView, mRateTextView, mExperienceTextView, mLongDescTextView;
    MapView mMapView;

    public DoctorProfileFragment() {
        // Required empty public constructor
    }

    public static DoctorProfileFragment newInstance(Doctor doctor) {
        DoctorProfileFragment fragment = new DoctorProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM_DOCTOR, doctor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            doctor = getArguments().getParcelable(ARG_PARAM_DOCTOR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_doctor_profile, container, false);
        mErrorTextView = (TextView) rootView.findViewById(R.id.text_area);
        mProfileNetworkImageView = (NetworkImageView) rootView.findViewById(R.id.image_profile);
        mSpecialityImageView = (ImageView) rootView.findViewById(R.id.image_speciality);
        mRatingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);
        mContentRelativeLayout = (RelativeLayout) rootView.findViewById(R.id.relative_content);
        mRecommendationTextView = (TextView) rootView.findViewById(R.id.text_recommendation);
        mNameTextView = (TextView) rootView.findViewById(R.id.text_name);
        mAreaTextView = (TextView) rootView.findViewById(R.id.text_area);
        mScheduleTextView = (TextView) rootView.findViewById(R.id.text_schedule);
        mCurrencyTextView = (TextView) rootView.findViewById(R.id.text_currency);
        mRateTextView = (TextView) rootView.findViewById(R.id.text_rate);
        mExperienceTextView = (TextView) rootView.findViewById(R.id.text_experience);
        mLongDescTextView = (TextView) rootView.findViewById(R.id.text_desc);
        mMapView = (MapView) rootView.findViewById(R.id.map);

        if (mMapView != null) {
            mMapView.onCreate(savedInstanceState);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        loadDoctorProfile();
        mImageLoader = RestApiClient.sharedInstance(getContext()).getImageLoader();

        return rootView;
    }

    private void loadDoctorProfile() {
        RestApiClient.sharedInstance(getContext())
                .getDoctor(doctor.getId(), new RestApiClient.OnGetDoctorProfileCompletedListener() {
                    @Override
                    public void onComplete(Doctor doctor, VolleyError error) {
                        if (error != null) {
                            displayErrorScreen();
                            Log.e(TAG, error.toString());
                        } else {
                            // TODO: UI touchup
                            String rate = String.valueOf(doctor.getRate());
                            rate = rate.substring(0, rate.indexOf("."));

                            mProfileNetworkImageView.setImageUrl(doctor.getImageUrl(), mImageLoader);

                            // assumption: max recommendation is 1000
                            float ratingSize= (float) (((doctor.getRecommendation())*5) /1000);
                            mRatingBar.setRating(ratingSize);

                            if (doctor.getSpeciality().equalsIgnoreCase("Dentist")) {
                                mSpecialityImageView.setImageResource(R.drawable.ic_dental);
                            }

                            mRecommendationTextView.setText(String.valueOf(doctor.getRecommendation()) + " recommendations");
                            mNameTextView.setText(doctor.getName());
                            mAreaTextView.setText(doctor.getArea());
                            mScheduleTextView.setText(doctor.getSchedule());
                            mCurrencyTextView.setText(doctor.getCurrency());
                            mRateTextView.setText(rate);
                            mExperienceTextView.setText(doctor.getExperience() + " years of experience");
                            mLongDescTextView.setText(doctor.getDescription());
                            mLatLng = new LatLng(doctor.getLatitude(), doctor.getLongitude());

                            displayNormalScreen();
                        }
                    }

                });
    }

    private void displayNormalScreen() {
        mErrorTextView.setVisibility(View.GONE);
        mContentRelativeLayout.setVisibility(View.VISIBLE);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateMarker();
            }
        });
    }

    private void displayErrorScreen() {
        mErrorTextView.setVisibility(View.VISIBLE);
        mContentRelativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
    }

    private void updateMarker() {
        mGoogleMap.addMarker(new MarkerOptions().position(mLatLng).title("My Clinic"));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 15));
    }

}
