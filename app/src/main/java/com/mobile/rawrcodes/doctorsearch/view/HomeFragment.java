package com.mobile.rawrcodes.doctorsearch.view;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.mobile.rawrcodes.doctorsearch.MainActivity;
import com.mobile.rawrcodes.doctorsearch.R;
import com.mobile.rawrcodes.doctorsearch.api.RestApiClient;
import com.mobile.rawrcodes.doctorsearch.model.LocationPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    public static String TAG = HomeFragment.class.getSimpleName();

    // List
    private List<LocationPoint> locationPointList = new ArrayList<>();

    // UI
    TextView mErrorTextView;
    AutoCompleteTextView mLocationAutoCompleteTextView;
    FloatingActionButton mSearchFAB;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    // todo: broadcast receiver to update list

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mErrorTextView = (TextView) rootView.findViewById(R.id.text_error);
        mLocationAutoCompleteTextView = (AutoCompleteTextView) rootView.findViewById(R.id.auto_complete_text);
        mSearchFAB = (FloatingActionButton) rootView.findViewById(R.id.fab_search_all);
        mSearchFAB.setOnClickListener(this);

        loadLocationPoint();
        return rootView;
    }

    private void loadLocationPoint() {
        RestApiClient.sharedInstance(getContext())
                .getLocationPoint(new RestApiClient.OnGetLocationPointCompletedListener() {
                    @Override
                    public void onComplete(List<LocationPoint> locationPoints, VolleyError error) {
                        if (error != null) {
                            displayErrorScreen();
                            Log.e(TAG, error.toString());
                        } else if (locationPoints.size() > 0) {
                            locationPointList.addAll(locationPoints);
                            String[] locations = new String[locationPointList.size()];
                            for (int i=0; i<locations.length; i++){
                                locations[i] = locationPointList.get(i).getArea() + ", "
                                        + locationPointList.get(i).getCity();
                            }
                            ArrayAdapter autoCompleteAdapter = new
                                    ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,
                                    locations);
                            mLocationAutoCompleteTextView.setAdapter(autoCompleteAdapter);
                            displayNormalScreen();
                        } else {
                            displayErrorScreen();
                            Log.e(TAG, "locationPoints is returned null or empty");
                        }
                    }

                });
    }

    private void displayNormalScreen() {
        mErrorTextView.setVisibility(View.GONE);
        mLocationAutoCompleteTextView.setVisibility(View.VISIBLE);
    }

    private void displayErrorScreen() {
        mErrorTextView.setVisibility(View.VISIBLE);
        mLocationAutoCompleteTextView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_search_all:
                DoctorsFragment fragment = DoctorsFragment.newInstance();

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(DoctorsFragment.TAG)
                        .replace(R.id.main_frame, fragment)
                        .commit();
                break;
        }
    }
}
