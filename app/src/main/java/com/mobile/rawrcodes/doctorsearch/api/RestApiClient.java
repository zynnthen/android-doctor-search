package com.mobile.rawrcodes.doctorsearch.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mobile.rawrcodes.doctorsearch.model.Doctor;
import com.mobile.rawrcodes.doctorsearch.model.LocationPoint;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Then on 21/11/2016.
 */

public class RestApiClient {
    private static RestApiClient sSharedInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static final String sBaseUrl = "http://52.76.85.10/test/";

    private RestApiClient(@NonNull Context context) {
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }

    public static synchronized RestApiClient sharedInstance(Context context) {
        if (sSharedInstance == null) {
            sSharedInstance = new RestApiClient(context);
        }

        return sSharedInstance;
    }

    public interface OnGetLocationPointCompletedListener {
        void onComplete(List<LocationPoint> locationPoints, VolleyError error);
    }

    public void getLocationPoint(final OnGetLocationPointCompletedListener completedListener) {
        String url = sBaseUrl + "location.json";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        List<LocationPoint> locationPoints = new ArrayList<>();

                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = (JSONObject) response.get(i);

                                String area = jsonObject.getString("area");
                                String city = jsonObject.getString("city");

                                LocationPoint locationPoint = new LocationPoint(jsonObject);
                                locationPoints.add(locationPoint);
                            }

                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        completedListener.onComplete(locationPoints, null);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completedListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public interface OnGetDoctorCompletedListener {
        void onComplete(List<Doctor> doctors, VolleyError error);
    }

    public void getDoctorList(final OnGetDoctorCompletedListener completedListener) {
        String url = sBaseUrl + "datalist.json";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>(){
                    @Override
                    public void onResponse(JSONArray response) {
                        List<Doctor> doctors = new ArrayList<>();

                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = (JSONObject) response.get(i);

                                Doctor doctor = new Doctor(jsonObject);
                                doctors.add(doctor);
                            }

                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        completedListener.onComplete(doctors, null);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completedListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public interface OnGetDoctorProfileCompletedListener {
        void onComplete(Doctor doctor, VolleyError error);
    }

    public void getDoctor(final String id, final OnGetDoctorProfileCompletedListener completedListener) {
        String url = sBaseUrl + "profile/" + id + ".json";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Doctor doctor = new Doctor(response);
                            completedListener.onComplete(doctor, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                completedListener.onComplete(null, error);
            }
        });

        mRequestQueue.add(request);
    }

    public ImageLoader getImageLoader(){
        return this.mImageLoader;
    }
}
