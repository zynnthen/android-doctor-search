package com.mobile.rawrcodes.doctorsearch.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.mobile.rawrcodes.doctorsearch.R;
import com.mobile.rawrcodes.doctorsearch.api.RestApiClient;
import com.mobile.rawrcodes.doctorsearch.model.Doctor;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoctorsFragment extends Fragment implements
        DoctorsAdapter.OnDoctorSelectedListener{
    public static String TAG = DoctorsFragment.class.getSimpleName();

    // List
    private List<Doctor> doctorList = new ArrayList<>();

    // Adapter
    private DoctorsAdapter mDoctorsAdapter;

    // UI
    TextView mErrorTextView;
    SeekBar mSeekBar;
    RecyclerView mDoctorRecyclerView;

    public DoctorsFragment() {
        // Required empty public constructor
    }

    public static DoctorsFragment newInstance() {
        DoctorsFragment fragment = new DoctorsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doctors, container, false);
        mErrorTextView = (TextView) rootView.findViewById(R.id.text_error);

        mSeekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                mDoctorsAdapter.setDisplaySize(progress);
                mDoctorsAdapter.notifyDataSetChanged();
            }
        });
        mDoctorsAdapter = new DoctorsAdapter(doctorList, getContext());
        mDoctorsAdapter.setDoctorListener(this);

        mDoctorRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycle_view_doctors);
        mDoctorRecyclerView.setAdapter(mDoctorsAdapter);

        loadDoctors();

        return rootView;
    }

    private void loadDoctors() {
        RestApiClient.sharedInstance(getContext())
                .getDoctorList(new RestApiClient.OnGetDoctorCompletedListener() {
                    @Override
                    public void onComplete(List<Doctor> doctors, VolleyError error) {
                        if (error != null) {
                            displayErrorScreen();
                            Log.e(TAG, error.toString());
                        } else if (doctors.size() > 0) {
                            doctorList.addAll(doctors);
                            mSeekBar.setMax(doctorList.size() - 1);
                            mSeekBar.setProgress(mSeekBar.getMax());
                            mDoctorsAdapter.notifyDataSetChanged();
                            displayNormalScreen();
                        } else {
                            displayErrorScreen();
                            Log.e(TAG, "doctors is returned null or empty");
                        }
                    }

                });
    }

    private void displayNormalScreen() {
        mErrorTextView.setVisibility(View.GONE);
        mDoctorRecyclerView.setVisibility(View.VISIBLE);
    }

    private void displayErrorScreen() {
        mErrorTextView.setVisibility(View.VISIBLE);
        mDoctorRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onDoctorSelected(Doctor doctor) {
        DoctorProfileFragment fragment = DoctorProfileFragment.newInstance(doctor);

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(DoctorProfileFragment.TAG)
                .replace(R.id.main_frame, fragment)
                .commit();
    }
}
